import Vue from 'vue';
import Vuex from 'vuex';
import app from './modules/app';
import user from './modules/user';


Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        app,
        user,
    },
});


// 检测 Webpack 的 HMR API
// https://doc.webpack-china.org/guides/hot-module-replacement/
if (module.hot) {
    const api = require('vue-hot-reload-api')
    const Vue = require('vue')

    // 将 API 安装到 Vue，并且检查版本的兼容性
    api.install(Vue)

    // 在安装之后使用 api.compatible 来检查兼容性
    if (!api.compatible) {
        throw new Error('vue-hot-reload-api与当前Vue的版本不兼容')
    }

    // 此模块接受热重载
    // 在这儿多说一句，webpack关于hmr的文档实在是太。。。
    // 各大框架的loader中关于hmr的实现都是基于自身模块接受更新来实现
    module.hot.accept()

    if (!module.hot.data) {
        // 为了将每一个组件中的选项变得可以热加载，
        // 你需要用一个不重复的id创建一次记录，
        // 只需要在启动的时候做一次。
        api.createRecord('very-unique-id', app)
    } else {
        // 如果一个组件只是修改了模板或是 render 函数，
        // 只要把所有相关的实例重新渲染一遍就可以了，而不需要销毁重建他们。
        // 这样就可以完整的保持应用的当前状态。
        api.rerender('very-unique-id', app)
        // --- 或者 ---
        // 如果一个组件更改了除 template 或 render 之外的选项，
        // 就需要整个重新加载。
        // 这将销毁并重建整个组件（包括子组件）。
        api.reload('very-unique-id', app)
    }
}
